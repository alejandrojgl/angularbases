import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  heroes: string[] = [];
  heroeDeleted: string = "";
  constructor() { }

  ngOnInit(): void {
    this.heroes = ['Capitan America','IronMan','Thor','Hulk'];
  }
  deletedHero(){
    // this.heroes.splice(0,1);
    this.heroeDeleted = this.heroes.shift() || '';
  }
}
