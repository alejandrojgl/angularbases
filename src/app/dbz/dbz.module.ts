import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../material.module';
import { DbzServices } from './services/dbz.services';

import { MainPageComponent } from './main-page/main-page.component';
import { CharacterComponent } from './character/character.component';
import { AddComponent } from './add/add.component';


@NgModule({
  declarations: [
    MainPageComponent,
    CharacterComponent,
    AddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DemoMaterialModule
  ],
  exports:[
    MainPageComponent
  ],
  providers:[
    DbzServices
  ]
})
export class DbzModule { }
