import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Character } from '../interfaces/dbz.interface';
import { DbzServices } from '../services/dbz.services';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {

  @Input() character:Character[] = [];
  @Input() new: Character = {
    name: '',
    power: 0
  };

  constructor( private dbzServices:DbzServices){

  }
  // @Output() onNewCharacter: EventEmitter<Character> = new EventEmitter();

  addform(){
    if( this.new.name.trim().length === 0 ){
      return;
    }

    this.dbzServices.addNewCharacter( this.new );

    // this.onNewCharacter.emit( this.new );

    this.new = {
      name: '',
      power: 0
    }
  }
}
