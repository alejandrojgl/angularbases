import { Component, OnInit } from '@angular/core';
import { Character } from '../interfaces/dbz.interface';
import { DbzServices } from '../services/dbz.services';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent {
  
  constructor(
    private dbzServices:DbzServices
  ){

  }

  new: Character = {
    name: 'Maestro Roshi',
    power: 1000
  };

}
