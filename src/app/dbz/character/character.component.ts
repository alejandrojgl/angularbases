import { Component, Input } from '@angular/core';
import { DbzServices } from '../services/dbz.services';


@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent {
  constructor(
    private dbzServices:DbzServices
  ){

  }
  get character() {
    return this.dbzServices.character;
  }
}
  // @Input() character: Character[] = [];

