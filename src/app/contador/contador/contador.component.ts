import { Component } from '@angular/core';

@Component({
	selector: 'app-contador',
	template: `
		<h1>Angular 2021</h1>
		<hr>
		<h2>{{ titulo }}</h2>
		<!-- <button (click)="numero = numero + 1;"> +1 </button> -->
		<h3>La base es : <strong>{{ base }}</strong></h3>

		<button (click)="acumular(base)"> + {{ base }} </button>

		<span> {{numero}} </span>

		<button (click)="acumular(-base)"> - {{ base }} </button>
    `
})
export class ContadorComponent {

	titulo: string = 'Contador APP';
  numero: number = 10;
  base: number = 5;
  
  acumular(valor:number){
   this.numero += valor; 
  }

}